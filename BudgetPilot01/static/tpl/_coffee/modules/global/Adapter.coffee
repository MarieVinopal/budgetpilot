$ = require('jquery')

# spracuje vsechny pozadavky ze serveru
adapter =
	init: ->
		# korektni odpoved - header code 200
		@ajaxSuccess()
		# chyby - header code 5xx
		@ajaxError();

	# defaultni akce pri poslani ajax pozadavku
	ajaxStart: ->
		# todo doplni loader

	# defaultni akce po prijeti ajax pozadavku ze serveru
	ajaxSuccess: ->
		$(document).ajaxSuccess (event, xhr, settings) ->
			# todo vypne loader

	# spracovani chbych odpovedi ze serveru, header <> 200
	ajaxError: ->
		$(document).ajaxError (event, xhr, settings) ->
			#if(xhr.responseText != undefined)
				#$('#snippet--flashMessages').html("<div class='alert alert-danger'><p><span class='glyphicon glyphicon glyphicon-remove'></span> <b>Error! </b>"+xhr.responseText+"</p></div>").children('div').delay(6000).fadeOut('slow');


	# mamualni odeslani pozadavku na server
	# @param string url
	# @param object data
	ajaxSend: (url, data, callback) ->
		$.nette.ajax(
				type: "POST",
				cache: false,
				url: url,
				data: data,
				complete: (data) ->
					if(callback)
						callback(data)
		)
	ajaxGet: (url, callback) ->
		$.nette.ajax(
			type: "GET",
			cache: false,
			url: url,
			complete: (data) ->
				if(callback)
					callback(data)
		)
module.exports = do ->

	initialized = false

	() ->

		#console.log commentsModule
		return adapter
