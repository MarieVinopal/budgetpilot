module.exports = do ->
	initialized = false
	running = false
	callbacks = []

	# run the actual callbacks
	runCallbacks = ->
		callbacks.forEach (callback) ->
			callback()
			return
		running = false
		return

	# adds callback to loop
	addCallback = (callback) ->

		if callback
			#console.log 'ajaxload add callback - ready'
			callbacks.push callback
		return

	#fix scroll to snippet #
	wPosition = 0
	$.nette.ext('winposition', {
		before: ->
			wPosition = $(window).scrollTop()
			return
		complete: ->
			$(window).scrollTop(wPosition)
			return
	})

	# fired on ajax.nette complete event
	$.nette.ext({
		complete:  (payload) ->
			runCallbacks()
	})

	(callback) ->

		if !initialized
			initialized = true
		#console.log 'ajaxload init - ready'
		addCallback callback

		return

