module.exports = do ->
	initialized = false
	running = false
	callbacks = []
	# fired on resize event

	resize = ->
		if !running
			running = true
			if window.requestAnimationFrame
				window.requestAnimationFrame runCallbacks
			else
				setTimeout runCallbacks, 66
		return

	# run the actual callbacks

	runCallbacks = ->
		callbacks.forEach (callback) ->
			callback()
			return
		running = false
		return

	# adds callback to loop

	addCallback = (callback) ->
		if callback
			callbacks.push callback
		return

	(opt) ->
		if opt.type == 'add'
			if !initialized
				window.addEventListener 'resize', resize
				initialized = true
			addCallback opt.callback
			return
			
		else if opt.type == 'fire'		
			#console.log 'manual fire'
			runCallbacks()
			return