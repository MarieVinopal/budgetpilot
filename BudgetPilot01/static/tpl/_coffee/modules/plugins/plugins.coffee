'use strict'
#attach plugins...

#require('admin-lte/plugins/datatables/jquery.dataTables.min.js')
require('admin-lte/plugins/datepicker/bootstrap-datepicker.js')


module.exports = do ->

    $('.datepicker').datepicker
        autoclose: true

###
    $('#example1').DataTable()
    $('#example2').DataTable
        'paging': true
        'lengthChange': false
        'searching': false
        'ordering': true
        'info': true
        'autoWidth': false
###