'use strict'

module.exports = do ->

	window.cookieconsent_options =
		"message": getLangText('common.cookieDisclaimer.message')
		"dismiss": getLangText('common.cookieDisclaimer.dismiss')
		"learnMore": getLangText('common.cookieDisclaimer.learnMore')
		"link": "//www.google.com/policies/technologies/cookies/"
		"theme": "dark-bottom"

	require('./cookieconsent.min.js')