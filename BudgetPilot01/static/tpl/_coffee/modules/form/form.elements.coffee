require('nette-ajax-module/nette.ajax.js');
$ = require('jquery');
icheck = require('icheck');
require('selectric/public/jquery.selectric.js')($)
onAjaxLoad = require('scripts/onAjaxLoad.coffee');
adapter = require('global/Adapter.coffee');
adapter = adapter()


require('bootstrap-datepicker');
require('bootstrap-datepicker/js/locales/bootstrap-datepicker.pl.js'); ##need lng!


formElements = {}

class FormElements

	constructor: (app)->
		_this = @
		@app = app


		return

	attachListener: () ->
		@attachSelectric()
		@attachIcheck()
		@attachFileBtn()
		@attachDatepicker()


		$('[data-toggle="tooltip"]').tooltip()
		
		return

	#select - clasic
	attachDatepicker: () ->
		$('input.datepicker').datepicker(
			format: 'dd.mm.yyyy'
			language: 'pl'
		);
	#select - clasic
	attachSelectric: () ->

		if !$('select').length then return false

		#https://github.com/lcdsantos/jQuery-Selectric
		$('select').each () ->

			tpl = $(this).data('tpl')
			openHover = $(this).data('hover')

			$(this).selectric(
				openOnHover: openHover
				disableOnMobile: false
				onInit: () ->
					if tpl then $(this).closest('.selectric-wrapper').addClass(tpl)
				onChange: ->
					$option = $(this).find('option:checked')
					if $(this).hasClass('ajax') && $option.data('target')
						adapter.ajaxSend($option.data('target'))
					else if $option.data('target')
						document.location = $option.data('target')


			)
	#checkbox, radios
	attachIcheck: () ->
		#https://github.com/fronteed/iCheck
		$("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)")
		.on 'ifCreated', (event) ->
			if $(this).data('tpl') then $(this).parent('div').addClass($(this).data('tpl')) #tpl for iCheck
		.on 'ifUnchecked', (event) ->

			if $(this).data('selfremove')
				$(this).closest('label').hide();


		.iCheck
			checkboxClass: 'icheckbox',
			radioClass: 'iradio'

	#filebtn
	#todo - data-btntext
	attachFileBtn: () ->

		if $('input[type="file"]:not(.redesigned)').lenght == 0 then return

		$(document).on 'change', '.btn-file :file', ->
			input = $(this)
			numFiles = if input.get(0).files then input.get(0).files.length else 1
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '')
			input.trigger 'fileselect', [
				numFiles
				label
			]
			return

		$(document).ready ->
			$('.btn-file :file').on 'fileselect', (event, numFiles, label) ->
				input = $(this).parents('.input-group').find(':text')
				log = if numFiles > 1 then numFiles + ' files selected' else label
				if input.length
					input.val log
				else
					if log
						alert log
				return
			return

		$html = '<div class="input-group">
					<input type="text" readonly="" class="form-control">
					<span class="input-group-btn">
						<span class="file-input btn btn-secondary btn-file">
							Vybrat obrázek …
						</span>
					</span>
				</div>'

		$('input[type="file"]').each ->
			$input = $(this)
			$input.addClass('redesigned')
			$input.before($html)
			$inputContainer = $input.prev()
			$inputContainer.find('.file-input').append($input)

module.exports = do ->

	initialized = false

	(options) ->

		if !initialized
			formElements = new FormElements(options)

			$(window).load ->
				formElements.attachListener()

			#called when is ajax content
			onAjaxLoad(formElements.attachSelectric)
			onAjaxLoad(formElements.attachIcheck)
			onAjaxLoad(formElements.attachFileBtn)
			onAjaxLoad(formElements.attachDatepicker)

			initialized = true


		#console.log commentsModule
		return formElements
