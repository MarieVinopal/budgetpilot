appConf =
	modules:
		#formElements:
			#module: require('form/form.elements.coffee')
			#options:
				## define modul components
				#selectric:
					#selector: 'data-selectric'
		#cookieDisclaimer:
			#module: require('cookie/CookieDisclaimer.coffee')
		bootstrap:
			require('bootstrap/dist/js/npm.js')
		adapter:
			module: require('global/Adapter.coffee')

        adminLte:
            module: require('admin-lte/dist/js/app.js')

		plugins:
			require('plugins/plugins.coffee')

		transactionsForm:
			require('transactionsForm/transactionsForm.coffee')

module.exports = appConf
