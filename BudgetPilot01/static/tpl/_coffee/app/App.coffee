'use strict'
$ = require('jquery');
jQuery = global.jQuery = $
global.$ = jQuery

#get app conf + fire no fuction modules (example nette extensions...)
appModules = require('./app-modules.coffee')
#appComponents = require('../../load.coffee')


class App

	constructor: (global) ->
        
	loadModules: (modulesConf, storeModules) ->
		#fire modules function...
		modules = {}

		for moduleName, moduleObj of modulesConf.modules

			module = moduleObj.module

			#console.log typeof module

			if typeof module == 'function'
				if moduleObj.options
					modules[moduleName] = module(moduleObj.options)
				else
					modules[moduleName] = module()

		#if appConf.debug then console.log 'module loaded: ' + moduleName

		return modules

	init: ->

        #store in window
        projectModules = @loadModules(appModules)
        #projectComponents = @loadModules(appComponents)

        
        @modules = $.extend({}, projectModules);
        console.log appModules
        #show stored modules
        console.log @modules

        @attachHandlers()



	attachHandlers: ->




module.exports = App