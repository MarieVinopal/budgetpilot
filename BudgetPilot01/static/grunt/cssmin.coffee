'use strict';
module.exports = 
	frontend:
		options:
			banner: '/* Frontend - <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
		files:
			'<%= paths.frontend %>css/<%= pkg.name %>-frontend.css': ['<%= paths.frontend %>css/<%= pkg.name %>-frontend.css']

