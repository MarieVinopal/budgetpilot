#npm update caniuse-db for update DB!
'use strict';
module.exports =
	orm:
		command: 'php www/index.php orm:schema-tool:update --force'
		options:
			stdout: true
	elastica:
		command: 'php www/index.php elastica:import:product'
		options:
			stdout: true
	tests:
		command: 'codecept run'
		options:
			stdout: true
	phing:
		command: 'phing'
		options:
			stdout: true