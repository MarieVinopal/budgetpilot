'use strict';
module.exports = 
    dist:
        src: [
            '<%= paths.frontend %>js/<%= pkg.name %>-frontend.js'
            '<%= paths.nodeModules %>/admin-lte/dist/js/app.js'
        ]
        dest: '<%= paths.frontend %>js/<%= pkg.name %>-frontend.js'
