'use strict';
module.exports = 
	options:
		preserveComments: 'some'
		banner: '/* <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
		compress:
			drop_console: true
		mangle: false	# Use if you want the names of your functions and variables unchanged
	frontend:				
		files:
			'<%= paths.frontend %>js/<%= pkg.name %>-frontend.js': '<%= paths.frontend %>js/<%= pkg.name %>-frontend.js'