#need: https://www.npmjs.com/package/coffeeify
'use strict';
module.exports =
	options:
		transform: ['coffeeify']
	frontend:
		files:
			'<%= paths.frontend %>js/<%= pkg.name %>-frontend.js': ['<%= paths.frontendSrc %>/_coffee/index.coffee']


