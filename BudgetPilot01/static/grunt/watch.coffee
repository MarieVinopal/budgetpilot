'use strict';

module.exports =
	options:
		nospawn: true
	patternlab:
		files: [
			'<%= paths.patternlab.source.patterns %>/**/*.mustache'
			'<%= paths.patternlab.source.patterns %>/**/*.json'
			'<%= paths.patternlab.source.data %>/*.json'
			'<%= paths.patternlab.source.root %>/*.ico'
		]
		tasks: [
			'patternlab:patternsonly'
			'bs-reload'
		]
	less:
		files: [
			'<%= paths.src %>/**/*.less'
		]
		tasks: ['less:frontend', 'copy:patternCss', 'bs-reload'] # tasks to run

	browserify:
		files: [
			'<%= paths.frontendSrc %>/**/*.coffee'
		]
		tasks: ['js-frontend-build', 'copy:patternJs', 'bs-reload'] # tasks to run