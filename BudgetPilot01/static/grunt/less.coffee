'use strict';
module.exports = 
	frontend:
		options:
			compress: true # minifying the result
		files:
			"<%= paths.frontend %>/css/<%= pkg.name %>-frontend.css":"<%= paths.frontendSrc %>/_less/base.less"
