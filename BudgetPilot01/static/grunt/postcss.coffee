'use strict';

processorArray = [
	require('autoprefixer')({ browsers: ['last 2 versions', 'ie 6-8', 'Firefox > 20']  })

];


module.exports =
	options:
		map: true
		processors: processorArray
	frontend:
		src: '<%= paths.frontend %>/css/*.css'

