'use strict';
path = require('path')

module.exports =

	## for static development
	patternlab:
		files: [
			{
				expand: true
				cwd:'<%= paths.patternlab.source.root %>'
				src: 'favicon.ico'
				dest: '<%= paths.patternlab.public.root %>'
			}
			{
				expand: true
				cwd: '<%= paths.patternlab.source.styleguide %>'
				src: [
					'*'
					'**'
				]
				dest: '<%= paths.patternlab.public.root %>'
			}
			{
				expand: true
				flatten: true
				cwd: path.resolve('<%= paths.patternlab.source.styleguide %>', 'styleguide', 'css', 'custom')
				src: '*.css)'
				dest: path.resolve('<%= paths.patternlab.public.styleguide %>', 'css')
			}
		]

	patternJs:
		files: [
			{expand: true, cwd: 'www/assets/frontend/js', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/frontend/js'}
		]

	patternCss:
		files: [
			{expand: true, cwd: 'www/assets/frontend/css', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/frontend/css'}
		]

	patternPublic:
		files: [
			{expand: true, cwd: 'www/assets/frontend/js', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/frontend/js'}
			{expand: true, cwd: 'www/assets/frontend/css', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/frontend/css'}
			{expand: true, cwd: 'www/assets/frontend/img', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/frontend/img'}
			{expand: true, cwd: 'www/assets/fonts', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/fonts'}
			{expand: true, cwd: 'www/assets/fonts', src: ['**'], dest: '<%= paths.patternlab.public.root %>/assets/frontend/fonts'}
		]
	## copy frontend  files (fonts, imgs,... another custom libs..)
	www:
		files: [
			{expand: true, cwd: '<%= paths.nodeModules %>/font-awesome/fonts/', src: ['**'], dest: '<%= paths.assets %>/fonts/'},
			{expand: true, cwd: '<%= paths.nodeModules %>/bootstrap/fonts/', src: ['**'], dest: '<%= paths.assets %>/fonts/'},
			{expand: true, cwd: '<%= paths.frontendSrc %>/_img/', src: ['**'], dest: '<%= paths.frontend %>/img/'}
			{expand: true, cwd: '<%= paths.frontendSrc %>/_fonts/', src: ['**'], dest: '<%= paths.assets %>/fonts/'}
		]
	buildAssets:
		files: [
			{expand: true, cwd: 'www/assets/frontend/js', src: ['**'], dest: '../Content/assets/frontend/js'}
			{expand: true, cwd: 'www/assets/frontend/css', src: ['**'], dest: '../Content/assets/frontend/css'}
			{expand: true, cwd: 'www/assets/frontend/img', src: ['**'], dest: '../Content/assets/frontend/img'}
			{expand: true, cwd: 'www/assets/fonts', src: ['**'], dest: '../Content/assets/fonts'}
			{expand: true, cwd: 'www/assets/fonts', src: ['**'], dest: '../Content/assets/frontend/fonts'}
		]