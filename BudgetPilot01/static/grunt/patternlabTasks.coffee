#npm update caniuse-db for update DB!
'use strict';
module.exports = (grunt) ->

	###*****************************************************
	# PATTERN LAB tasks
	*****************************************************
	###
	config = require('../tpl/_pattern/patternlab-config.json')
	argv = require('minimist')(process.argv.slice(2))
	pl = require('patternlab-node')(config)

	getConfiguredCleanOption = ->
		config.cleanPublic

	paths = ->
		config.paths

	grunt.registerTask 'patternlab', 'create design systems with atomic design', (arg) ->
		if arguments.length == 0
			pl.build (->
			), getConfiguredCleanOption()
		if arg and arg == 'version'
			pl.version()
		if arg and arg == 'patternsonly'
			pl.patternsonly (->
			), getConfiguredCleanOption()
		if arg and arg == 'help'
			pl.help()
		if arg and arg == 'starterkit-list'
			pl.liststarterkits()
		if arg and arg == 'starterkit-load'
			pl.loadstarterkit argv.kit
		if arg and arg != 'version' and arg != 'patternsonly' and arg != 'help' and arg != 'starterkit-list' and arg != 'starterkit-load'
			pl.help()
		return
		
	grunt.registerTask 'patternlab:build', [
		'clean:patternlab'
		'patternlab'
		'copy:patternlab'
		'frontend'
		'copy:patternPublic'
	]
	grunt.registerTask 'patternlab:watch', [
		'patternlab:build'
		'watch'
	]
	grunt.registerTask 'patternlab:serve', [
		'patternlab:build'
		'bs-init'
		'watch'
	]
