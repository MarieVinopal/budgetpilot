'use strict';
module.exports = (grunt) ->

	#FRONTEND TASKS
	###############
	#JS - frontend

	grunt.task.registerTask 'js-frontend-build', ' ', ->
		process.env["NODE_PATH"] = 'tpl/_coffee/modules';
		grunt.task.run [
			'browserify:frontend'
            #'concat:dist'
		]
	grunt.registerTask 'js-frontend', [
		'js-frontend-build'
	]

	#CSS - frontend
	grunt.registerTask 'css-frontend', [
		'less:frontend'
		'cmq:frontend'
		'cssmin:frontend'
		'postcss:frontend'
	]
	grunt.registerTask 'frontend', [
		'css-frontend' #generate css to asset
		'js-frontend' #generate js to asset
        'copy'  #copy img, fonts to asset
	]