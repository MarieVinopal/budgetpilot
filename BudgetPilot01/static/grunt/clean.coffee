#npm update caniuse-db for update DB!
'use strict';
module.exports = 
	netteCash: 
		src: [
			"temp/cache/*"
			"temp/proxies/*"
		]
	patternlab:
		src: [
			"<%= paths.patternlab.public.root %>"
		]
	assets: 
		src: [
			"<%= paths.assets %>"
		] 