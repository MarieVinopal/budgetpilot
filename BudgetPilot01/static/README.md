##CQB-patternlab

Devstack na přípravu šablon (frontendu) pro projekty postavené na CQB.

## Instalace

node dep
```
npm install
```
build theme
```
grunt

```

## Patternlab server (livereload)

start serveru
```
grunt patternlab:serve

```


##CSS
Globální .less a jeho konfigurace se nastavují v `tpl/_less`
Základní import .less probíhá klasicky v `tpl/_less/base.less`

.less pro .mustache komponenty se udržuje vedle sebe a mají stejný název.

Například:
```
product-box.mustache
product-box.less
```

pro .less component platí, že `product-box.less` obsahuje vždy :
```
.product-box {


	//when you need states
	&.is-active {

	}

	//when you need templating
	&.product-box-example-large {

	}
}

```

Import component .less probíhá pomocí  `tpl/load.less`


##JS
Píše se v coffescriptu a .js se staví pomocí **browserify**

Nastavení js a jeho konfigurace probíhá v `tpl/_coffee/app/App.coffee`
Obecné moduly se loadují přes `tpl/_coffee/app/app-modules.coffee`

JS moduly pro componenty mají stejný název jako .mustache (.less) soubor

Například:
```
product-box.coffee
product-box.mustache
product-box.less
```