'use strict';

###*****************************************************
# PATTERN LAB NODE
# EDITION-NODE-GRUNT
# The grunt wrapper around patternlab-node core, providing tasks to interact with the core library and move supporting frontend assets.
*****************************************************
###

browserSync = require("browser-sync");
module.exports = (grunt) ->

	path = require('path')

	devGrunt = require("./gruntfile-conf")
	config = require('./tpl/_pattern/patternlab-config.json')

	options =
		pkg: grunt.file.readJSON('package.json')
		paths:
			assets: "www/assets"
			frontend: "www/assets/frontend/"
			frontendSrc: "tpl/"
			vendor: "vendor"
			nodeModules: "node_modules"
			src: 'tpl'
			logs: "log"
			filesystem: "file:///" + require("path").resolve()
			patternlab: config.paths
		config:
			src: 'grunt'


	#grunt.log.write(options.paths.patternlab.source.css)

	# Load grunt configurations automatically + tasks
	require('load-grunt-config') grunt,
		configPath: [
			path.join(process.cwd(), options.config.src)
		]
		data: options


	##dev
	grunt.registerTask 'bs-init', ->
		done = @async()
		browserSync(
			devGrunt.browserSync
		, (err, bs) ->
			done()
			return
		)
		return

	grunt.registerTask "bs-reload", ->
		grunt.log.write('run reload page')
		browserSync.reload();

	#Default "grunt" + minify .js
	grunt.registerTask 'build', [
		'clean'
		'patternlab'
		'frontend'
		'uglify'
        'copy:buildAssets'
	]

	grunt.registerTask 'default', [
		'build'
	]


	return
