﻿using Microsoft.Owin;
using Owin;
using System.Web.Services.Description;

[assembly: OwinStartupAttribute(typeof(BudgetPilot01.Startup))]
namespace BudgetPilot01
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
           
        }

        public void ConfigureServicies(ServiceCollection services)
        {
        }
        
    }
}
