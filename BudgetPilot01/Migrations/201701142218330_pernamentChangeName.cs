namespace BudgetPilot01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pernamentChangeName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "IsFromPernamentTemplate", c => c.Boolean(nullable: false));
            DropColumn("dbo.Transactions", "Periodical");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "Periodical", c => c.Boolean(nullable: false));
            DropColumn("dbo.Transactions", "IsFromPernamentTemplate");
        }
    }
}
