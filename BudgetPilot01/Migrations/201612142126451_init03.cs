namespace BudgetPilot01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init03 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PernamentTransactionTemplates",
                c => new
                    {
                        PernamentTransactionTemplateID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Count = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CategoryID = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Owner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PernamentTransactionTemplateID)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_Id)
                .ForeignKey("dbo.TransactionCategories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.Owner_Id);
            
            CreateTable(
                "dbo.TransactionCategories",
                c => new
                    {
                        TransactionCategoryID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsForIncome = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionCategoryID);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        TransactionID = c.Int(nullable: false, identity: true),
                        Count = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Name = c.String(),
                        CategoryID = c.Int(nullable: false),
                        Periodical = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        TransactionCode = c.String(),
                        Owner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TransactionID)
                .ForeignKey("dbo.TransactionCategories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_Id)
                .Index(t => t.CategoryID)
                .Index(t => t.Owner_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        StartingBudget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Nick = c.String(),
                        CurrencyID = c.Int(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TransactionsCurrencies", t => t.CurrencyID, cascadeDelete: true)
                .Index(t => t.CurrencyID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.TransactionsCurrencies",
                c => new
                    {
                        TransactionsCurrencyID = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        ShortName = c.String(),
                    })
                .PrimaryKey(t => t.TransactionsCurrencyID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.PernamentTransactionTemplates", "CategoryID", "dbo.TransactionCategories");
            DropForeignKey("dbo.AspNetUsers", "CurrencyID", "dbo.TransactionsCurrencies");
            DropForeignKey("dbo.Transactions", "Owner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PernamentTransactionTemplates", "Owner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Transactions", "CategoryID", "dbo.TransactionCategories");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "CurrencyID" });
            DropIndex("dbo.Transactions", new[] { "Owner_Id" });
            DropIndex("dbo.Transactions", new[] { "CategoryID" });
            DropIndex("dbo.PernamentTransactionTemplates", new[] { "Owner_Id" });
            DropIndex("dbo.PernamentTransactionTemplates", new[] { "CategoryID" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TransactionsCurrencies");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Transactions");
            DropTable("dbo.TransactionCategories");
            DropTable("dbo.PernamentTransactionTemplates");
        }
    }
}
