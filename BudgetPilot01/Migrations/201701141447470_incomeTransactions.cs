namespace BudgetPilot01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class incomeTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "IsIncome", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "IsIncome");
        }
    }
}
