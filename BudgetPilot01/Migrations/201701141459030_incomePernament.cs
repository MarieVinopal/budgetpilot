namespace BudgetPilot01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class incomePernament : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PernamentTransactionTemplates", "IsIncome", c => c.Boolean(nullable: false));
            DropColumn("dbo.TransactionCategories", "IsForIncome");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TransactionCategories", "IsForIncome", c => c.Boolean(nullable: false));
            DropColumn("dbo.PernamentTransactionTemplates", "IsIncome");
        }
    }
}
