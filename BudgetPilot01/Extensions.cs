﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace BudgetPilot01
{
    public static class Extensions
    {
        public static string ToStringValueENG(this bool value)
        {
            return value ? "Yes" : "No";
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return String.IsNullOrEmpty(value);
        }

        public static decimal FormatUS(this decimal value)
        {
            return Convert.ToDecimal(value, new System.Globalization.CultureInfo("en-US"));
        }
    }

}