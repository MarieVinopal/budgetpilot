﻿function DrawChartIO(canvas, incomes, outcomes) {
    var ctx = document.getElementById(canvas);
    var myPieChart = new Chart(ctx, {
        type: 'pie',           
        data: {
            labels: [
                "Incomes",
                "Outcomes",
            ],
            datasets: [
                {
                    data: [incomes, outcomes],
                    backgroundColor: [
                        "#007400",
                        "#CA0000",
                    ],
                        
                }]
                
        },
        options: {
            responsive: true
        }
    });
}

function DrawChartPernament(canvas, numPeriodical, numOneTime) {
    var ctx = document.getElementById(canvas);
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [
        "Periodical",
        "One-Time",
            ],
            datasets: [
                {
                    data: [numPeriodical, numOneTime],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ]
                }]
        },
        options: {
            responsive: true
        }
    });
}