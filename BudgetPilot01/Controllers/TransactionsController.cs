﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BudgetPilot01.Models;
using BudgetPilot01.Data;
using System.Globalization;

namespace BudgetPilot01.Controllers
{
    [Authorize]
    public class TransactionsController : Controller
    {
        private ITransactionsRepository TransactionsRepository;
        private ICategoriesRepository CategoriesRepository;
        private IUsersRepository UsersRepository;

        public TransactionsController()
        {
            TransactionsRepository = new TransactionsRepository();
            UsersRepository = new UsersRepository();
            CategoriesRepository = new CategoriesRepository();
        }

        // GET: Transactions
        [HttpGet]
        public ActionResult Index()
        {
            
            ViewBag.Months = Months();

            var firstYearCall = TransactionsRepository.FirstTransactionYear(User.Identity.Name);
            ViewBag.Years = Years(firstYearCall);

            return View();
        }

        // GET: Transactions/PernamentTransactions
        [HttpGet]
        public ActionResult PernamentTransactions()
        {
            var OneTimeTransactionsCountCall = TransactionsRepository.SummaryOneTimeTransactions(User.Identity.Name);
            var PernamentTransactionsCountCall = TransactionsRepository.SummaryPernamentTransactions(User.Identity.Name);
            var PernamentTransactionTemplatesCall = TransactionsRepository.PernamentTransactionsTemplatesAll(User.Identity.Name, typeof(PernamentTransactionDetailVM));

            if (OneTimeTransactionsCountCall.Code == RepositoryResultCode.Ok && PernamentTransactionsCountCall.Code == RepositoryResultCode.Ok 
                && PernamentTransactionTemplatesCall.Code == RepositoryResultCode.Ok)
            {
                ViewBag.OneTimeCount = OneTimeTransactionsCountCall.Content;
                ViewBag.PernamentCount = PernamentTransactionsCountCall.Content;

                IEnumerable<PernamentTransactionDetailVM> PernamentTransactions = PernamentTransactionTemplatesCall.Content as IEnumerable<PernamentTransactionDetailVM>;
                return View(PernamentTransactions);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        //Partial into Index
        [HttpGet]
        public ActionResult TransactionOverview(string Month, string Year)
        {
          
            if (Month.IsNullOrEmpty() || Year.IsNullOrEmpty())
            {
                Month = DateTime.Now.Month.ToString();
                Year = DateTime.Now.Year.ToString();
            }

            var TransactionsCall = TransactionsRepository.UsersTransactionsMoreSelection(typeof(Transaction), User.Identity.Name, Month, Year);
            Accountant accountant = GenerateAccountant(Month, Year);
            if(accountant != null)
            {
                ViewBag.Account = accountant;
                return PartialView("_TransactionOverview", ViewModelConvertor.ToTransactionDetailVMs(TransactionsCall.Content as IEnumerable<Transaction>)); 
            }

            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        //POST Trigger for tranfor pernament transactions templates to real transactions
        [HttpPost]
        [AllowAnonymous]
        public ActionResult TransactionsFromTemplates([System.Web.Http.FromBody]string innerToken)
        {

            if (Services.AppSecurityService.CheckTokenForInternalCall(innerToken))
            {
                TransactionsRepository.CreateTransactionsFromTemplates();
                return new HttpStatusCodeResult(HttpStatusCode.Created); 
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        }

        #region Create New Transactions + Templates
        // GET: Transactions/CreateNewTransaction
        [HttpGet]
        public ActionResult CreateNewTransaction()
        {
            ViewBag.Categories = LoadCategories(CategoriesRepository, IncludeTransactions: false);
            TransactionCreateVM newCreateVM = new TransactionCreateVM() { IsIncome = true, Created = null};
            
            return View(newCreateVM);
        }

        // POST: Transactions/CreateNewTransaction
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewTransaction(TransactionCreateVM TransactionCreateVM)
        {
            if (ModelState.IsValid)
            {
                TransactionsRepository.AddNewTransaction(TransactionCreateVM, User.Identity.Name);
                return RedirectToAction("Index");
            }

            ViewBag.Categories = LoadCategories(CategoriesRepository, IncludeTransactions: false);
            return View(TransactionCreateVM);
        }

        // GET: Transactions/CreateNewPernamentTransaction
        [HttpGet]
        public ActionResult CreateNewPernamentTransaction()
        {
            PernamentTransactionCreateVM CreateVM = new PernamentTransactionCreateVM() { IsIncome = true, SendFirstTransaction = true };
            ViewBag.Categories = LoadCategories(CategoriesRepository, IncludeTransactions: false);

            return View(CreateVM);
        }

        // POST: Transactions/CreateNewPernamentTransaction
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewPernamentTransaction(PernamentTransactionCreateVM CreateVM)
        {
            if (ModelState.IsValid)
            {
                TransactionsRepository.AddNewPernamentTransaction(CreateVM, User.Identity.Name);
                return RedirectToAction("PernamentTransactions");
            }

            ViewBag.Categories = LoadCategories(CategoriesRepository, IncludeTransactions: false);
            return View(CreateVM);
        }
        #endregion



        // DELETE: Transactions/RemoveSelectedTransaction/5
        [HttpDelete]
        public ActionResult RemoveSelectedTransaction(int? id)
        {
            if (id == null || !Request.UrlReferrer.OriginalString.Contains("/Transactions"))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var result = TransactionsRepository.DeleteTransaction(User.Identity.Name, id.Value);

            if(result.Code ==  RepositoryResultCode.Ok)
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            else
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

        }

        // DELETE: Transactions/RemoveSelectedPernamentTransaction/5
        [HttpDelete]
        public ActionResult RemoveSelectedPernamentTransaction(int? id)
        {
            if (id == null || !Request.UrlReferrer.OriginalString.Contains("/Transactions/PernamentTransactions"))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var result = TransactionsRepository.DeletePernamentTransaction(User.Identity.Name, id.Value);

            if (result.Code == RepositoryResultCode.Ok)
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            else
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        #region Inner controller methods

        /// <summary>
        /// Load transaction categories
        /// </summary>
        /// <param name="rep"></param>
        /// <param name="IncludeTransactions"></param>
        /// <returns></returns>
        protected IList<SelectListItem> LoadCategories(ICategoriesRepository rep, bool IncludeTransactions)
        {
            var repositoryResult = rep.CategoriesAll(IncludeTransactions);
            var categories = repositoryResult.Content as IEnumerable<TransactionCategory>;
            IList<SelectListItem> result = new List<SelectListItem>();
            foreach (var category in categories)
            {
                result.Add(new SelectListItem() { Text = category.Name, Value = category.TransactionCategoryID.ToString() });
            }
            return result;
        }

        /// <summary>
        /// Return select list of named months
        /// </summary>
        /// <returns></returns>
        protected SelectList Months()
        {
            string[] monthNames = DateTimeFormatInfo.InvariantInfo.MonthNames;
            IList<SelectListItem> months = new List<SelectListItem>();
            var actualMonthNumber = DateTime.Now.Month.ToString();

            int MonthNumber = 1;

            foreach (var month in monthNames)
            {
                if (month.IsNullOrEmpty())
                    break;

                if (MonthNumber.ToString().Equals(actualMonthNumber))
                    months.Add(new SelectListItem() { Text = month, Value = MonthNumber.ToString(), Selected = true });
                else
                    months.Add(new SelectListItem() { Text = month, Value = MonthNumber.ToString() });

                MonthNumber++;
            }

            var selectedItem = months.First(m => m.Selected);

            SelectList result = new SelectList(months, "Value", "Text", months.IndexOf(selectedItem) + 1);
            return result;
        }

        /// <summary>
        /// Create select list of years, when was user registered
        /// </summary>
        /// <param name="firstYear"></param>
        /// <returns></returns>
        protected SelectList Years(string firstYear)
        {
            IList<SelectListItem> years = new List<SelectListItem>();
            int yearInt = Convert.ToInt32(firstYear);
            int currentYear = Convert.ToInt32(DateTime.Now.Year);
            do
            {
                years.Add(new SelectListItem() { Text = yearInt.ToString(), Value = yearInt.ToString() });
                yearInt++;
            }
            while (yearInt <= currentYear);

            return new SelectList(years, "Value", "Text", years.FirstOrDefault(y => y.Value.Equals(DateTime.Now.Year.ToString())));
        } 

        /// <summary>
        /// Generate accountant from repository calls
        /// </summary>
        /// <param name="Month"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        private Accountant GenerateAccountant(string Month, string Year)
        {
            var IncomesOutcomesCall = TransactionsRepository.SummaryIncomesOutcomes(User.Identity.Name);
            var TransactionsCall = TransactionsRepository.UsersTransactionsMoreSelection(typeof(Transaction), User.Identity.Name, Month, Year);
            var CurrencyCall = UsersRepository.UsersCurrency(User.Identity.Name);
            var StartingBudgetCall = UsersRepository.UsersBudget(User.Identity.Name);


            if (TransactionsCall.Code == RepositoryResultCode.Ok && IncomesOutcomesCall.Code == RepositoryResultCode.Ok && CurrencyCall.Code == RepositoryResultCode.Ok)
            {
                Accountant Accountant = new Accountant(
                    startingBudget: (decimal)StartingBudgetCall.Content,
                    monthTransactions: ViewModelConvertor.ToAccountantViewModelsVMs(TransactionsCall.Content as IEnumerable<Transaction>),
                    trasactionsSum: IncomesOutcomesCall.Content as Dictionary<string, decimal>,
                    usedCurrency: CurrencyCall.Content as TransactionsCurrency
                    );

                return Accountant;
            }
            else
                return null;
        }
        #endregion
    }
}
