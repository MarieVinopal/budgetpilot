﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BudgetPilot01.Models;

namespace BudgetPilot01.Data
{
    public class TransactionsRepository : ITransactionsRepository
    {
       

        #region Currency
        /// <summary>
        /// Return all currency values
        /// </summary>
        /// <param name="IncludeUsers"></param>
        /// <returns></returns>
        public RepositoryResult CurrencyAll(bool IncludeUsers)
        {
            using (var db = new ApplicationDbContext())
            {
                if (IncludeUsers)
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = db.TransactionCurrencies.Include(c => c.UsedByUsers).ToList() };
                else
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = db.TransactionCurrencies.ToList() };
            }
        }

        /// <summary>
        /// Find specific currency
        /// </summary>
        /// <param name="currencyID"></param>
        /// <param name="includeUsers"></param>
        /// <returns></returns>
        public RepositoryResult CurrencyByID(int currencyID, bool includeUsers)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    if (includeUsers)
                        return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = db.TransactionCurrencies.Include(c => c.UsedByUsers).First(c => c.TransactionsCurrencyID == currencyID) };
                    else
                        return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = db.TransactionCurrencies.First(c => c.TransactionsCurrencyID == currencyID) };
                }
                catch (Exception)
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.NotFound, Content = null };
                }
            }
        } 
        #endregion

        /// <summary>
        /// Resturns year of the first transaction
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public string FirstTransactionYear(string userName)
        {
            using (var db = new ApplicationDbContext())
            {
                var dates = db.Transactions.Where(t => t.Owner.UserName.Equals(userName)).OrderBy(t => t.Created).Select(t => t.Created).ToList();
                if (dates.Any())
                    return dates.FirstOrDefault().Year.ToString();
                else
                    return DateTime.Now.Year.ToString();
            }
        }

       
        /// <summary>
        /// Returns transactions if the user from specific month
        /// </summary>
        /// <param name="RequestedType"></param>
        /// <param name="Owner"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public RepositoryResult UsersTransactionsMoreSelection(Type RequestedType, string Owner, string month, string year)
        {
            using (var db = new ApplicationDbContext())
            {
                IEnumerable<Transaction> result;

                result = db.Transactions
                         .Include(t => t.Owner)
                         .Include(t => t.Category)
                         .Where(t => t.Owner.UserName.Equals(Owner))
                         .Where(t => t.Created.Month.ToString().Equals(month) && t.Created.Year.ToString().Equals(year))
                         .OrderBy(t => t.Created)
                         .ToList();

                if (RequestedType == typeof(Transaction))
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = result };
                else if (RequestedType == typeof(TransactionDetailVM))
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = ViewModelConvertor.ToTransactionDetailVMs(result) };
                else if (RequestedType == typeof(TransactionAccountantVM))
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = ViewModelConvertor.ToAccountantViewModelsVMs(result) };
                else
                    return new RepositoryResult() { Code = RepositoryResultCode.UnknownType, Content = null };
            }
        }

        /// <summary>
        /// Add new transaction from view model
        /// </summary>
        /// <param name="newTransactionVM"></param>
        /// <param name="ownerName"></param>
        /// <returns></returns>
        public RepositoryResult AddNewTransaction(TransactionCreateVM newTransactionVM, string ownerName)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var categoryResult = db.TransactionCategories.Find(newTransactionVM.CategoryIdentificator);
                    var ownerResult = db.Users.Where(u => u.UserName.Equals(ownerName)).FirstOrDefault();
                    Transaction newTransaction = Transaction.CreateNewTransactionFromUsersData(newTransactionVM, ownerResult, categoryResult);
                    db.Transactions.Add(newTransaction);
                    int resultContent = db.SaveChanges();

                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = 1
                    };
                }
                catch (Exception exc)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.RepositoryException,
                        Content = exc.Message
                    };
                }

            }
        }

        /// <summary>
        /// Returns all users pernament transaction templates
        /// </summary>
        /// <param name="Owner"></param>
        /// <param name="RequestedType"></param>
        /// <returns></returns>
        public RepositoryResult PernamentTransactionsTemplatesAll(string Owner, Type RequestedType)
        {
            using (var db = new ApplicationDbContext())
            {
                var templates = db.PernamentTransactionTemplates.Include(p=>p.Category).Where(t => t.Owner.UserName.Equals(Owner)).ToList();
                if (RequestedType == typeof(PernamentTransactionTemplate))
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = templates
                    }; 
                }
                else if (RequestedType == typeof(PernamentTransactionDetailVM))
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = ViewModelConvertor.ToPernamentTransactionDetailVMs(templates)
                    };
                }
                else
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.UnknownType,
                        Content = null
                    };
                }
            
            }
        }

        /// <summary>
        /// Add new pernament transaction template
        /// </summary>
        /// <param name="createVM"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public RepositoryResult AddNewPernamentTransaction(PernamentTransactionCreateVM createVM, string owner)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var categoryResult = db.TransactionCategories.Find(createVM.CategoryIdentificator);
                    var ownerResult = db.Users.Where(u => u.UserName.Equals(owner)).FirstOrDefault();
                    PernamentTransactionTemplate pernamentTransacrionNew = PernamentTransactionTemplate.CreateNewFromVM(createVM, ownerResult, categoryResult);
                    db.PernamentTransactionTemplates.Add(pernamentTransacrionNew);
                    int resultContent = db.SaveChanges();

                    if (createVM.SendFirstTransaction)
                    {
                        db.Transactions.Add(new Transaction()
                        {
                            CategoryID = createVM.CategoryIdentificator,
                            Count = createVM.Count,
                            Created = DateTime.Now,
                            IsIncome = createVM.IsIncome,
                            Name = createVM.Name,
                            Owner = ownerResult,
                            IsFromPernamentTemplate = true,
                            TransactionCode = BudgetPilot01.Services.AppSecurityService.GenerateRandomCode(16)
                        });
                        db.SaveChanges();
                    }

                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = 1
                    };
                }
                catch (Exception exc)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.RepositoryException,
                        Content = exc.Message
                    };
                }

            }
        }

        /// <summary>
        /// Returns summary income from all transactions
        /// </summary>
        /// <param name="Owner"></param>
        /// <returns></returns>
        public RepositoryResult SummaryIncomes(string Owner)
        {
            using (var db = new ApplicationDbContext())
            {
                decimal result = 0M;
                var IncomeTransactions = db.Transactions
                    .Include(t => t.Category)
                    .Where(t => t.Owner.UserName.Equals(Owner))
                    .Where(t => t.IsIncome)
                    .Select(s => s.Count);
                if(IncomeTransactions.Count() > 0)
                    result += IncomeTransactions.Sum();
                return new RepositoryResult()
                {
                    Code = RepositoryResultCode.Ok,
                    Content = result
                };                
            }
        }

        /// <summary>
        /// Return summary outcome from all transactions
        /// </summary>
        /// <param name="Owner"></param>
        /// <returns></returns>
        public RepositoryResult SummaryOutcomes(string Owner)
        {
            using (var db = new ApplicationDbContext())
            {
                decimal result = 0M;
                var OutcomeTransactions = db.Transactions
                    .Include(t => t.Category)
                    .Where(t => t.Owner.UserName.Equals(Owner))
                    .Where(t => !t.IsIncome)
                    .Select(s => s.Count);
                if (OutcomeTransactions.Count() > 0)
                    result += OutcomeTransactions.Sum();
                return new RepositoryResult()
                {
                    Code = RepositoryResultCode.Ok,
                    Content = result
                };
            }
        }

        /// <summary>
        /// Return summary incomes and outcomes like a dictionary
        /// </summary>
        /// <param name="Owner"></param>
        /// <returns></returns>
        public RepositoryResult SummaryIncomesOutcomes(string Owner)
        {
            using (var db = new ApplicationDbContext())
            {
                decimal IncomesResult = 0.0M;
                decimal OutcomesResult = 0.0M;

                var Incomes = db.Transactions
                    .Include(t => t.Category)
                    .Where(t => t.Owner.UserName.Equals(Owner))
                    .Where(t => t.IsIncome)
                    .Select(s => s.Count);
                var Outcomes = db.Transactions
                    .Include(t => t.Category)
                    .Where(t => t.Owner.UserName.Equals(Owner))
                    .Where(t => !t.IsIncome)
                    .Select(s => s.Count);

                if (Incomes.Count() > 0)
                {
                    IncomesResult += Incomes.Sum();
                }
                if (Outcomes.Count() > 0)
                {
                    OutcomesResult += Outcomes.Sum();
                }


                var result = new Dictionary<string, decimal>();
                result.Add("incomes", IncomesResult);
                result.Add("outcomes", OutcomesResult);

                return new RepositoryResult()
                {
                    Code = RepositoryResultCode.Ok,
                    Content = result
                };
            }
        }

        /// <summary>
        /// Delete specific transaction
        /// </summary>
        /// <param name="Owner"></param>
        /// <param name="identificator"></param>
        /// <returns></returns>
        public RepositoryResult DeleteTransaction(string Owner, int identificator)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var result = db.Transactions.First(t => t.Owner.UserName.Equals(Owner) && t.TransactionID == identificator);
                    db.Transactions.Remove(db.Transactions.Find(result.TransactionID));
                    db.SaveChanges();

                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = null
                    };
                }
                catch (Exception)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.NotFound,
                        Content = null
                    };
                }
            }
        }

        /// <summary>
        /// Delete specific pernament transaction template
        /// </summary>
        /// <param name="Owner"></param>
        /// <param name="identificator"></param>
        /// <returns></returns>
        public RepositoryResult DeletePernamentTransaction(string Owner, int identificator)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var result = db.PernamentTransactionTemplates.First(t => t.Owner.UserName.Equals(Owner) && t.PernamentTransactionTemplateID == identificator);
                    db.PernamentTransactionTemplates.Remove(db.PernamentTransactionTemplates.Find(result.PernamentTransactionTemplateID));
                    db.SaveChanges();

                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = null
                    };
                }
                catch (Exception)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.NotFound,
                        Content = null
                    };
                }
            }
        }

        /// <summary>
        /// Create new transactions from saved pernament transaction templates
        /// </summary>
        /// <returns></returns>
        public RepositoryResult CreateTransactionsFromTemplates()
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {

                    IList<PernamentTransactionTemplate> templates = db.PernamentTransactionTemplates
                        .Include(t => t.Category)
                        .Include(t => t.Owner)
                        .ToList();

                    foreach(PernamentTransactionTemplate template in templates)
                    {
                        db.Transactions.Add(Transaction.CreateNewTransactionFromPrenamentTransactionTemplate(template));
                    }

                    int resultContent = db.SaveChanges();

                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = 1
                    };
                }
                catch (Exception exc)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.RepositoryException,
                        Content = exc.Message
                    };
                }

            }
        }

        /// <summary>
        /// Return number of created transactions coming from pernament templates
        /// </summary>
        /// <param name="Owner"></param>
        /// <returns></returns>
        public RepositoryResult SummaryPernamentTransactions(string Owner)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var result = db.Transactions.Where(t => t.Owner.UserName.Equals(Owner) && t.IsFromPernamentTemplate);
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = result.Count()
                    };
                }
                catch (Exception)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.RepositoryException,
                        Content = null
                    };
                }
            }
        }

        /// <summary>
        /// Return number of one-time transactions
        /// </summary>
        /// <param name="Owner"></param>
        /// <returns></returns>
        public RepositoryResult SummaryOneTimeTransactions(string Owner)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var result = db.Transactions.Where(t => t.Owner.UserName.Equals(Owner) && t.IsFromPernamentTemplate == false);
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = result.Count()
                    };
                }
                catch (Exception)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.RepositoryException,
                        Content = null
                    };
                }
            }
        }
    }
}