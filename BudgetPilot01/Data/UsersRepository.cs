﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BudgetPilot01.Models;

namespace BudgetPilot01.Data
{
    public class UsersRepository : IUsersRepository
    {

        /// <summary>
        /// Return what currency is user using
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public RepositoryResult UsersCurrency(string userName)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var user = db.Users.Include(u => u.UsingCurrency).FirstOrDefault(u => u.UserName.Equals(userName));
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = user.UsingCurrency };
                }

                catch
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.NotFound, Content = null };
                }
            }
        }

        /// <summary>
        /// Return users starting budget
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public RepositoryResult UsersBudget(string userName)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var user = db.Users.FirstOrDefault(u => u.UserName.Equals(userName));
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = user.StartingBudget };
                }

                catch
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.NotFound, Content = null };
                }
            }
        }

        /// <summary>
        /// Return users nick
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public RepositoryResult UsersNick(string userName)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var user = db.Users.FirstOrDefault(u => u.UserName.Equals(userName));
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = user.Nick };
                }

                catch
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.NotFound, Content = null };
                }
            }
        }

        /// <summary>
        /// Return date of users registration
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public RepositoryResult UsersRegistrationDate(string userName)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var user = db.Users.FirstOrDefault(u => u.UserName.Equals(userName));
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = user.RegistrationDate};
                }

                catch
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.NotFound, Content = null };
                }
            }
        }

        /// <summary>
        /// Users profile
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public RepositoryResult UsersCurrentProfile(string userName)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var user = db.Users.FirstOrDefault(u => u.UserName.Equals(userName));
                    return new RepositoryResult() {
                        Code = RepositoryResultCode.Ok,
                        Content = new EditUsersProfile()
                        {
                            CurrencyIdentificator = user.CurrencyID,
                            Nick = user.Nick,
                            StartingBudget = user.StartingBudget
                        }
                    };
                }

                catch
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.NotFound, Content = null };
                }
            }
        }


        /// <summary>
        /// Save current users profile status
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="newProfile"></param>
        /// <returns></returns>
        public RepositoryResult EditUsersProfile(string userName, EditUsersProfile newProfile)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var user = db.Users.FirstOrDefault(u => u.UserName.Equals(userName));
                    user.StartingBudget = newProfile.StartingBudget;
                    user.Nick = newProfile.Nick;
                    user.CurrencyID = newProfile.CurrencyIdentificator;
                    db.Entry<ApplicationUser>(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = null };
                }

                catch
                {
                    return new RepositoryResult() { Code = RepositoryResultCode.UnknownError, Content = null };
                }
            }
        }
    }
}