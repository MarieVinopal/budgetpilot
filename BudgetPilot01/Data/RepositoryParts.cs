﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudgetPilot01.Data
{
    public class RepositoryResult
    {
        public RepositoryResultCode Code { get; set; }
        public object Content { get; set; }
    }

    public enum RepositoryResultCode
    {
        Ok,
        NotFound,
        UnknownError,
        UnknownType,
        RepositoryException
    }


    public class RepositoryException : Exception
    {
        public RepositoryException(string message) : base(message) { }
    }

}