﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity.Migrations;
using BudgetPilot01.Models;

namespace BudgetPilot01.Data
{
    

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TransactionCategory> TransactionCategories { get; set; }
        public virtual DbSet<TransactionsCurrency> TransactionCurrencies { get; set; }
        public virtual DbSet<PernamentTransactionTemplate> PernamentTransactionTemplates { get; set; }


        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }


    public interface IApplicationDbContext
    {
        DbSet<Transaction> Transactions { get; set; }
        DbSet<TransactionCategory> TransactionCategories { get; set; }
        DbSet<TransactionsCurrency> TransactionCurrencies { get; set; }
        DbSet<PernamentTransactionTemplate> PernamentTransactionTemplates { get; set; }

    }
}