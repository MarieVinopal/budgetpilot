﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BudgetPilot01.Models;
using System.Globalization;
using BudgetPilot01.Models;

namespace BudgetPilot01.Data
{
    public class CategoriesRepository : ICategoriesRepository
    {

        /// <summary>
        /// Find specific category
        /// </summary>
        /// <param name="name"></param>
        /// <param name="includeTransactions"></param>
        /// <param name="ResquestedType"></param>
        /// <returns></returns>
        public RepositoryResult FindCategoryByName(string name, bool includeTransactions, Type ResquestedType)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    TransactionCategory category;
                    if (includeTransactions)
                        category = db.TransactionCategories.Include(c => c.Transactions).FirstOrDefault(c => c.Name.Equals(name));
                    else
                        category = db.TransactionCategories.FirstOrDefault(c => c.Name.Equals(name));

                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = category
                    };
                }
                catch (Exception)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.NotFound,
                        Content = null
                    };
                }
            }
        }

        /// <summary>
        /// Find specific category by ID
        /// </summary>
        /// <param name="identificator"></param>
        /// <param name="includeTransactions"></param>
        /// <param name="ResquestedType"></param>
        /// <returns></returns>
        public RepositoryResult FindCategoryByID(int identificator, bool includeTransactions, Type ResquestedType)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    TransactionCategory category;
                    if (includeTransactions)
                        category = db.TransactionCategories.Include(c => c.Transactions).FirstOrDefault(c => c.TransactionCategoryID == identificator);
                    else
                        category = db.TransactionCategories.FirstOrDefault(c => c.TransactionCategoryID == identificator);


                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.Ok,
                        Content = category
                    };
                }
                catch (Exception)
                {
                    return new RepositoryResult()
                    {
                        Code = RepositoryResultCode.NotFound,
                        Content = null
                    };
                }
            }
        }

        /// <summary>
        /// Return all categories
        /// </summary>
        /// <param name="IncludeTransactions"></param>
        /// <returns></returns>
        public RepositoryResult CategoriesAll(bool IncludeTransactions)
        {
            using (var db = new ApplicationDbContext())
            {
                if (IncludeTransactions)
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = db.TransactionCategories.Include(c => c.Transactions).ToList() };
                else
                    return new RepositoryResult() { Code = RepositoryResultCode.Ok, Content = db.TransactionCategories.ToList() };
            }
        }

    }
}