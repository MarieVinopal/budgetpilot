﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BudgetPilot01.Models;

namespace BudgetPilot01.Data
{
    public interface ITransactionsRepository
    {
        RepositoryResult UsersTransactionsMoreSelection(Type RequestedType, string Owner, string month, string year);
        RepositoryResult PernamentTransactionsTemplatesAll(string Owner, Type RequestedType);
        RepositoryResult AddNewTransaction(TransactionCreateVM newTransactionVM, string owner);
        RepositoryResult AddNewPernamentTransaction(PernamentTransactionCreateVM createVM, string owner);
        RepositoryResult CurrencyAll(bool IncludeUsers);
        RepositoryResult CurrencyByID(int currencyID, bool includeUsers);
        RepositoryResult SummaryIncomes(string Owner);
        RepositoryResult SummaryOutcomes(string Owner);
        RepositoryResult SummaryIncomesOutcomes(string Owner);
        RepositoryResult SummaryPernamentTransactions(string Owner);
        RepositoryResult SummaryOneTimeTransactions(string Owner);
        RepositoryResult DeleteTransaction(string Owner, int identificator);
        RepositoryResult DeletePernamentTransaction(string Owner, int identificator);
        RepositoryResult CreateTransactionsFromTemplates();
        string FirstTransactionYear(string userName);
    }

    public interface ICategoriesRepository
    {
        RepositoryResult FindCategoryByName(string name, bool includeTransactions, Type ResquestedType);
        RepositoryResult FindCategoryByID(int identificator, bool includeTransactions, Type ResquestedType);
        RepositoryResult CategoriesAll(bool IncludeTransactions);
    }

        public interface IUsersRepository
     {
        RepositoryResult UsersCurrency(string userName);

        RepositoryResult UsersBudget(string userName);

        RepositoryResult UsersNick(string userName);

        RepositoryResult UsersRegistrationDate(string userName);

        RepositoryResult UsersCurrentProfile(string userName);

        RepositoryResult EditUsersProfile(string userName, EditUsersProfile newProfile);
    }
}
