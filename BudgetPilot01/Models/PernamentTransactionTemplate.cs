﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudgetPilot01.Models
{
    public class PernamentTransactionTemplate
    {
        [Key]
        public int PernamentTransactionTemplateID { get; set; }
        public string Name { get; set; }
        public decimal Count { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }
        public virtual TransactionCategory Category { get; set; }

        public bool IsIncome { get; set; }

        public virtual ApplicationUser Owner { get; set; }
        public bool Active { get; set; }

        public PernamentTransactionDetailVM DetailVM
        {
            get
            {
                return new PernamentTransactionDetailVM()
                {
                    Identificator = PernamentTransactionTemplateID,
                    CategoryName = Category.Name,
                    Name = Name,
                    Active = Active.ToStringValueENG(),
                    Count = Count
                };
            }
        }

        public static PernamentTransactionTemplate CreateNewFromVM(PernamentTransactionCreateVM createVM, ApplicationUser ownerResult, TransactionCategory categoryResult)
        {
            return new PernamentTransactionTemplate()
            {
                Category = categoryResult,
                Name = createVM.Name,
                Owner = ownerResult,
                Count = createVM.Count,
                Active = true,
                IsIncome = createVM.IsIncome
            };
        }
    }

    public class PernamentTransactionDetailVM
    {
        public int Identificator { get; set; }
        public string Name { get; set; }
        public decimal Count { get; set; }
        public string CategoryName { get; set; }
        public string Active { get; set; }
        public bool IsIncome { get; set; }
    }
    public class PernamentTransactionCreateVM
    {
        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        public decimal Count { get; set; }

        [Required]
        [DisplayName("Select Category")]
        public int CategoryIdentificator { get; set; }

        [Required]
        [DisplayName("Income Transaction")]
        public bool IsIncome { get; set; }

        [Required]
        [DisplayName("Send First Transaction")]
        public bool SendFirstTransaction { get; set; }

    }

}