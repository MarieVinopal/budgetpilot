﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BudgetPilot01.Models
{
    public class TransactionsCurrency
    {
        [Key]
        public int TransactionsCurrencyID { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public virtual IList<ApplicationUser> UsedByUsers { get; set; }

        public string SelectListFormat
        {
            get
            {
                return FullName + " (" + ShortName + ")";
            }
        }

        public int NumberOfUsersUsingThisCurrency
        {
            get { return UsedByUsers.Count; }
        }
    }
}