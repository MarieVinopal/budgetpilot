﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace BudgetPilot01.Models
{
    public class TransactionDetailVM
    {
        public int TransactionID { get; set; }
        public decimal Count { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Periodical { get; set; }
        public string Created { get; set; }
        public string TransactionCode { get; set; }
        public bool IsIncome { get; set; }
    }

    public class TransactionAccountantVM
    {
        public int TransactionIdentificator { get; set; }
        public decimal Count { get; set; }
        public string Name { get; set; }
        public TransactionCategory Category { get; set; }
        public bool Periodical { get; set; }
        public DateTime Created { get; set; }
        public string TransactionCode { get; set; }
        public bool IsIncome { get; set; }
    }

    public class TransactionCreateVM
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must set up count")]
        [DisplayName("Count:")]
        [Range(Double.MinValue, Double.MaxValue, ErrorMessage = "Must be a number")]

        public decimal Count { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "You must set up transaction name")]
        [DisplayName("Transaction Name:")]
        [MinLength(3, ErrorMessage = "Name must have three or more characters")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Select Category:")]
        public int CategoryIdentificator { get; set; }

        [Required]
        [DisplayName("Income Transaction")]
        public bool IsIncome { get; set; }

        [Required]
        [DisplayName("Date:")]
        public DateTime? Created { get; set; }
    }

}