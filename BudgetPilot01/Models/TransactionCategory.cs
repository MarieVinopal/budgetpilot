﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BudgetPilot01.Models
{
    public class TransactionCategory
    {
        [Key]
        public int TransactionCategoryID { get; set; }
        public string Name { get; set; }
        public virtual IList<Transaction> Transactions { get; set; }
        public virtual IList<PernamentTransactionTemplate> PernamentTransactions { get; set; }

    }
}