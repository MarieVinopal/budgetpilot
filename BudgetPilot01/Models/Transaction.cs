﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudgetPilot01.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionID { get; set; }
        public decimal Count { get; set; }
        public string Name { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }
        public virtual TransactionCategory Category { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public bool IsFromPernamentTemplate { get; set; }
        public DateTime Created { get; set; }
        public string TransactionCode { get; set; }
        public bool IsIncome { get; set; }

        #region View Models Convertion
        public TransactionDetailVM ViewModelDetail
        {
            get
            {
                return new TransactionDetailVM
                {
                    TransactionID = TransactionID,
                    Category = Category.Name,
                    Created = CreatedDateFormated,
                    Name = Name,
                    Count = Count,
                    Periodical = IsFromPernamentTemplate.ToStringValueENG(),
                    TransactionCode = TransactionCode,
                    IsIncome = IsIncome
                };
            }
        }

        public TransactionAccountantVM ViewModelAccountant
        {
            get
            {
                return new TransactionAccountantVM
                {
                    TransactionIdentificator = this.TransactionID,
                    Category = this.Category,
                    Count = Count,
                    Created = Created,
                    Name = Name,
                    Periodical = IsFromPernamentTemplate,
                    TransactionCode = TransactionCode,
                    IsIncome = IsIncome
                };
            }
        } 
        #endregion


        private string CreatedDateFormated
        {
            get
            {
                return Created.ToShortDateString();
            }
        }

        #region Create New Transaction Methods
        public static Transaction CreateNewTransactionFromUsersData(TransactionCreateVM createVM, ApplicationUser owner, TransactionCategory category)
        {

            return new Transaction()
            {
                Count = createVM.Count,
                Created = createVM.Created.Value,
                Owner = owner,
                Name = createVM.Name,
                IsFromPernamentTemplate = false,
                TransactionCode = BudgetPilot01.Services.AppSecurityService.GenerateRandomCode(16),
                Category = category,
                IsIncome = createVM.IsIncome
            };
        }

        public static Transaction CreateNewTransactionFromPrenamentTransactionTemplate(PernamentTransactionTemplate template)
        {

            return new Transaction()
            {
                Count = template.Count,
                Created = DateTime.Now,
                Owner = template.Owner,
                Name = template.Name,
                IsFromPernamentTemplate = true,
                TransactionCode = BudgetPilot01.Services.AppSecurityService.GenerateRandomCode(16),
                Category = template.Category,
                IsIncome = template.IsIncome
            };
        } 
        #endregion
    }

   
    
}