﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BudgetPilot01.Models
{
    public class Accountant : IAccountantService
    {
        readonly decimal StartingBudget;
        readonly IEnumerable<TransactionAccountantVM> MonthTransactionColletion;
        readonly IDictionary<string, decimal> IncomesOutcomesSummaryAllTime;
        readonly TransactionsCurrency UsedCurrency;

        public Accountant(decimal startingBudget, IEnumerable<TransactionAccountantVM> monthTransactions, IDictionary<string, decimal> trasactionsSum, TransactionsCurrency usedCurrency)
        {
            StartingBudget = startingBudget;
            MonthTransactionColletion = monthTransactions;
            UsedCurrency = usedCurrency;
            IncomesOutcomesSummaryAllTime = trasactionsSum;
        }

        public IEnumerable<TransactionAccountantVM> AllIncomeTransactions
        {
            get
            {
                return MonthTransactionColletion.Where(t => t.IsIncome).ToList();
            }
        }

        public IEnumerable<TransactionAccountantVM> AllOutcomeTransactions
        {
            get
            {
                return MonthTransactionColletion.Where(t => !t.IsIncome).ToList();
            }
        }

        public decimal SummaryBalance
        {
            get
            {
                return (StartingBudget + IncomesOutcomesSummaryAllTime["incomes"] - IncomesOutcomesSummaryAllTime["outcomes"]).FormatUS();
            }
        }

        public string SummaryBalanceFormated
        {
            get
            {
                return Math.Round(SummaryBalance, 2).ToString() + " " + UsedCurrency.ShortName;
            }
        }

        public decimal MonthIncomeBalance
        {
            get
            {
                decimal result = 0.0M;
                foreach(var transaction in AllIncomeTransactions)
                {
                    result += transaction.Count;
                }
                return result.FormatUS();
            }
        }
        public decimal MonthOutcomeBalance
        {
            get
            {
                decimal result = 0.0M;
                foreach (var transaction in AllOutcomeTransactions)
                {
                    result += transaction.Count;
                }
                return result.FormatUS();
            }
        }

        public string MonthIncomeBalanceFormated
        {
            get
            {
                return MonthIncomeBalance.ToString() + " " + UsedCurrency.ShortName;
            }
        }
        public string MonthOutcomeBalanceFormated
        {
            get
            {
                return MonthOutcomeBalance.ToString() + " " + UsedCurrency.ShortName;

            }
        }

        public decimal MonthBalance
        {
            get
            {
                return (MonthIncomeBalance - MonthOutcomeBalance).FormatUS();

            }
        }

        public string MonthBalanceFormated
        {
            get
            {
                return MonthBalance + " " + UsedCurrency.ShortName;

            }
        }

        public int NumberOfPeriodicalTransactions
        {
            get
            {
                return MonthTransactionColletion.Where(t => t.Periodical).Count();
            }
        }

        public decimal SumOfPeriodicalTransactions
        {
            get
            {
                return MonthTransactionColletion.Where(t => t.Periodical).Select(t=>t.Count).Sum();
            }
        }

        public int NumberOfNonPeriodicalTransactions
        {
            get
            {
                return MonthTransactionColletion.Where(t => !t.Periodical).Count();
            }
        }

        public decimal SumOfNonPeriodicalTransactions
        {
            get
            {
                return MonthTransactionColletion.Where(t => !t.Periodical).Select(t => t.Count).Sum();
            }
        }

    }

    public interface IAccountantService
    {
        IEnumerable<TransactionAccountantVM> AllIncomeTransactions { get; }
        IEnumerable<TransactionAccountantVM> AllOutcomeTransactions { get; }

        decimal SummaryBalance
        {
            get;          
        }
        string SummaryBalanceFormated
        {
            get;
        }

        decimal MonthIncomeBalance
        {
            get;           
        }
        decimal MonthOutcomeBalance
        {
            get;
        }

        string MonthIncomeBalanceFormated
        {
            get;
            
        }
        string MonthOutcomeBalanceFormated
        {
            get;           
        }

        decimal MonthBalance
        {
            get;
        }

        string MonthBalanceFormated
        {
            get;
        }

        int NumberOfPeriodicalTransactions
        {
            get;
        }

        decimal SumOfPeriodicalTransactions
        {
            get;           
        }

        int NumberOfNonPeriodicalTransactions
        {
            get;
        }

        decimal SumOfNonPeriodicalTransactions
        {
            get;
        }
    }
}