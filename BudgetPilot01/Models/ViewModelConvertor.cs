﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudgetPilot01.Models
{
    public static class ViewModelConvertor
    {
        public static IEnumerable<TransactionDetailVM> ToTransactionDetailVMs(IEnumerable<Transaction> Transactions)
        {
            foreach(var transaction in Transactions)
            {
                yield return transaction.ViewModelDetail;
            }

        }

        public static IEnumerable<PernamentTransactionDetailVM> ToPernamentTransactionDetailVMs(IEnumerable<PernamentTransactionTemplate> PernamentTransactions)
        {
            foreach (var transaction in PernamentTransactions)
            {
                yield return transaction.DetailVM;
            }
        }

        public static IEnumerable<TransactionAccountantVM> ToAccountantViewModelsVMs(IEnumerable<Transaction> Transactions)
        {
            foreach (var transaction in Transactions)
            {
                yield return transaction.ViewModelAccountant;
            }
        }
    }
}