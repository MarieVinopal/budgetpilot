﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace BudgetPilot01.Services
{
    public class AppSecurityService
    {
        public static bool CheckTokenForInternalCall(string token)
        {
            if (token.IsNullOrEmpty())
                return false;
            else
                return token.Equals(ConfigurationManager.AppSettings["validationToken"].ToString());
        }

        public static string GenerateRandomCode(int bytesLenght)
        {
            byte[] result = new byte[bytesLenght];
            using (var generator = new RNGCryptoServiceProvider())
            {
                generator.GetBytes(result);
            }

            return BitConverter.ToString(result);
        }
    }

}