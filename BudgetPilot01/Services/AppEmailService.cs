﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using BudgetPilot01.Models;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace BudgetPilot01.Services
{
    public static class AppEmailService
    {

        public static void SendEmail(MailMessage message)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Send(message);
            }
        }

    }

    public static class EmailMessagesGenerator
    {

        public static MailMessage RegistrationCompleted(string callbackUrl, string nick, string email)
        {
            MailDefinition definition = new MailDefinition();
            definition.IsBodyHtml = true;
            definition.Subject = "Registration - Verify Email";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("{nick}", nick);
            replacements.Add("{callback}", callbackUrl);

            string mailBody = "Hi {nick}, <br/>"
                            + "To complete registration please press <a href='{callback}'>This link</a> to verify this email." + EmailSign();

            return definition.CreateMailMessage(email, replacements, mailBody, new System.Web.UI.Control());
        }

        public static MailMessage ForgotPassword(string callbackUrl, string nick, string email)
        {
            MailDefinition definition = new MailDefinition();
            definition.IsBodyHtml = true;
            definition.Subject = "Reset Password";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("{nick}", nick);
            replacements.Add("{callback}", callbackUrl);

            string mailBody = "Hi {nick}, <br/>"
                            + "To reset password please press <a href='{callback}'>This link</a> to verify this email." + EmailSign();

            return definition.CreateMailMessage(email, replacements, mailBody, new System.Web.UI.Control());
        }

        private static string EmailSign()
        {
            return "<br/><br/> Your BudgetPilot Team";
        }
    }
}