﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionID { get; set; }
        public string Name { get; set; }
    }
}